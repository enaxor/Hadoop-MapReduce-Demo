package eu.rxsoft.shgz;

import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.log4j.Logger;

public class SncfWages {

    static Logger log = Logger.getLogger(SncfWages.class.getName());

    public static class WageAvgMapper
        extends Mapper<Object, Text, Text, IntWritable> {
        private Text position = new Text();

        @Override
        public void map(Object key, Text value, Context context)
            throws IOException, InterruptedException {
            String[] fields = value.toString().split(";");
            IntWritable wage = new IntWritable(0);
            if (fields.length == 4) {
                // skip header line
                if (!fields[0].equals(" Date")) {
                    // get contract line
                    position.set(fields[1]);
                    wage.set(Integer.parseInt(fields[3]));
                    // commit result
                    context.write(position, wage);
                }
            }
        }
    }

    public static class WageAvgReducer
        extends Reducer<Text, IntWritable, Text, IntWritable> {
        private IntWritable result = new IntWritable();

        @Override
        public void reduce(Text key, Iterable<IntWritable> values, Context context)
            throws IOException, InterruptedException {
            int totalWage = 0;
            int positionIteration = 0;
            // compute total wage for this job position
            for (IntWritable value : values) {
                totalWage += value.get();
                ++positionIteration;
            }
            // compute average wage
            if (positionIteration > 1) {
                result.set(totalWage/positionIteration);
            } else {
                result.set(totalWage);
            }
            // commit result
            context.write(key, result);
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        // perform command line option parsing
        String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        if (otherArgs.length < 2) {
            System.err.println("Usage: sncfwages <in> <out>");
            System.exit(2);
        }
        log.info("Input file: " + otherArgs[0] + " Output file: " + otherArgs[1]);
        // init job
        Job job = Job.getInstance(conf, "sncfwages");
        job.setJobName("Sncf agents average wages");
        job.setJarByClass(SncfWages.class);
        // mapper
        job.setMapperClass(WageAvgMapper.class);
        // reducer
        job.setCombinerClass(WageAvgReducer.class);
        job.setReducerClass(WageAvgReducer.class);
        // output types
        job.setOutputKeyClass(Text.class); // position
        job.setOutputValueClass(IntWritable.class); // wage
        // input / output files
        FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
        FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
        // launch job
        boolean status = job.waitForCompletion(true);
        // get job progress duration
        long duration = job.getFinishTime() - job.getStartTime();
        log.info("Job elapsed time: " + duration / 1000 + " sec.");
        // launch job and exit
        System.exit(status ? 0 : 1);
    }
}
