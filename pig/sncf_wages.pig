datas = LOAD 'data_sncf.csv' USING PigStorage(';') as (year:int, contract:chararray, quality:chararray, wage:int);
orderedWages = ORDER datas BY wage ASC;
groupedContracts = GROUP orderedWages BY contract;
result = FOREACH groupedContracts GENERATE group, AVG(orderedWages.wage);
STORE result INTO 'sncf_wage.result';
