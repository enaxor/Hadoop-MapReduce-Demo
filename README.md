Hadoop MapReduce Demo
======================

A demo using [open datas](http://www.data.gouv.fr/fr/datasets/remuneration-mensuelle-moyenne-brute-des-agents-sncf/) to test Hadoop mapreduce feature :elephant: and pig latin :pig:.

**How to run**
* Install and configure [Hadoop](https://hadoop.apache.org/docs/stable/index.html)

* Clone repo on your hadoop server
```sh
$ git clone git@github.com:xarone/Hadoop-MapReduce-Demo.git
```

#### Java mapreduce
* Create jar
```sh
$ cd mapreduce
$ mvn package 
```
Assuming your server is located at http://hadoop/ with user 'rsp'.
* Copy csv file to server
```sh
hadoop -fs -put ~/data_sncf.csv /user/rsp/
```
* Run job
```sh
hadoop jar ~/Hadoop-MapReduce-Demo/mapreduce/hadoop-mapreduce-sncf-*.jar "/user/rsp/data_sncf.csv" "/user/rsp/output"
```
* Read output
```sh
hadoop fs -cat /user/rsp/output/part-r-00000
```

#### Pig Latin
```sh
cd Hadoop-MapReduce-Demo/pig
./runme.sh
```

**Expected results**

| Job Position  | Average Mensual Wage |
| :--- | ---: |
| Agents-au-statut | 3428 |
| Agents-contractuels | 3473 |
| Cadres-supérieurs | 7254 |
